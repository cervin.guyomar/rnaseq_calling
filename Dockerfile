
FROM nfcore/base:1.12.1

# Install the conda environment
COPY environment.yml /
RUN conda env create --quiet -f /environment.yml && conda clean -a

# Add conda installation dir to PATH (instead of doing 'conda activate')
ENV PATH /opt/conda/envs/1000rnaseq_calling/bin:$PATH

RUN bash -c 'wget -O script.tar https://forgemia.inra.fr/cervin.guyomar/workflows/-/archive/master/workflows-master.tar?path=Snakemake/1000RNASeq_chicken/calling/script'
RUN bash -c 'mkdir script && tar -xf script.tar -C ./script'
RUN bash -c 'find ./script/*/ -type f -exec mv -vi "{}" ./script/ \;'
RUN bash -c 'rm -rf ./script/*/'
RUN bash -c 'rm script.tar'

RUN bash -c 'ln -s /usr/local/src/cutadapt_summary.py /usr/local/bin'
RUN bash -c 'ln -s /usr/local/src/rsem_summary.py /usr/local/bin'
RUN bash -c 'ln -s /usr/local/src/star_summary.py /usr/local/bin'
RUN bash -c 'ln -s /usr/local/src/trimgalore_summary.py /usr/local/bin'

# Register gatk
RUN bash -c 'wget https://storage.googleapis.com/gatk-software/package-archive/gatk/GenomeAnalysisTK-3.7-0-gcfedb67.tar.bz2'
RUN bash -c 'mv GenomeAnalysisTK-3.7-0-gcfedb67.tar.bz2 /usr/local/src/GenomeAnalysisTK-3.7-0-gcfedb67.tar.bz2'
RUN bash -c 'gatk-register /usr/local/src/GenomeAnalysisTK-3.7-0-gcfedb67.tar.bz2'
